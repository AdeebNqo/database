libs = ".:lib/mysql-connector-java-5.1.18-bin.jar"
test.class: test.java Database.class
	@javac test.java
Database.class: Database.java
	@javac -cp $(libs) Database.java
run:
	@java -cp $(libs) test
clean:
	@rm Database.class test.class
